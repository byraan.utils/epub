// file
const fs=require('fs'),
path=require('path');
// custom global
const commandLineArgs=require('../lib/command-line-args'),
log=require('../lib/log'),
exec=require('../lib/exec');
cli: {
	commandLineArgs(this, [], 1);
	process.arguments.input=this.subcommands[0];
	if(!fs.existsSync(process.arguments.input)){
		log('{{error}} File not found');
		return;
	}
	let fileData=path.parse(process.arguments.input);
	if(fileData.ext!='.epub'){
		log('{{error}} File not is epub type');
		return;
	}
}
let lib=path.resolve(`${__dirname}/../resources/epubcheck-4.2.4`);
exec(
	`java -jar "${lib}/epubcheck.jar" "${process.arguments.input}"`
);